<style type="text/css">
h1 {
	color: darkred;
}

h2 {
	color: red;
}

h3 {
	color: blue;
}

p, ul, ol {
    margin-left: 30px;
    margin-right: 20px;
}

code, tt {
	font-family: Courier, fixed;
	font-weight: bold;
	color: brown;
}
</style>

# La barre d'outils ![barre d'outils](./img/toolbar.fr.png) #

* ![Bouton d'aide](./img/help.png)Le premier bouton à gauche de la barre d'outil fait apparaître une fenêtre « à propos » de cette application, et remet le présent document sur le dessus.
* Ensuite, le bouton des outils![Bouton d'outils](./img/gnome-run.png) donne accès aux outil de maintenance des clés Freeduc : réinitialisation si quelqu'un les a rendues inconsistantes, mais aussi sauvegarde des données de persistance.
* Le bouton de lancement, quant à lui, ![Bouton de lancement](./img/computer.png), permet de lancer le système de la clé Freeduc dans une machine virtuelle (si on a installé le paquet qemu-kvm).
* L'application centrale, c'est le clonage de clés USB, ![Bouton de clonage](./img/jumping-gnu-48x48.png), qu'on peut lancer avec le bouton ou à l'aide du titre juste à côté.
* Le dernier bouton, tout à droite, permet de quitter l'application. ![Bouton pour quitter](./img/application-exit.png) On peut quitter même quand une commande est en cours et que les moniteurs en affichent les détails.

# Le déroulement d'un clonage #

Dans l'afficheur juste sous la barre d'outils, on peut voir des détails sur tous les disques USB détectés, qui sont connectés à l'ordinateur. Sous GNU-Linux, leur désignation est ``/dev/sdX`` où ``X`` désigne une lettre de l'alphabet ; ces lettres sont attribuées en général selon la chronologie des branchements.

Quand on démarre un clonage ![Bouton de clonage](./img/jumping-gnu-48x48.png), on peut cocher plusieurs disques à la fois ; selon la qualité de l'ordinateur, de ses ports USB et des clés, on peut avoir un avantage à en cloner plusieurs à la fois. 

Quand le ou les disques sont choisis, il faut désigner l'image Freeduc qu'on veut y inscrire. Si cette application fonctionne *depuis une clé Freeduc*, l'option par défaut est **AUTOCLONE**, c'est à dire clonage de la clé elle-même sur les clés-cibles. On peut aussi choisir, dans tous les cas, une image de disque ISO, qui doit être présente dans le système de fichiers.

Il suffit alors de valider tous ces choix et le clonage commence. Il se déroule en quelques minutes, et il est possible d'en suivre la progression dans autant de moniteurs qu'il y a de disques en cours de clonage. L'opération la plus longue est la copie de l'image du cœur du système, qui assure le démarrage du noyau Linux, et la présence de centaines de programmes, fournis par un système de fichiers compressé, en lecture-seule. Quand cette opération se termine, une partition dite de *persistance* est créée, c'est celle qui contiendra toutes les modifications apportées à la clé durant « sa vie » : les données visibles de l'utilisateur, ainsi que nombre de fichiers cachés, liés à des configurations, et aussi les nouveaux programmes qu'on désire y installer.

Quand l'opération de clonage se termine, il est possible d'enregistrer les messages des moniteurs à toutes fins utiles.

# La logique des outils de maintenance #

Il est plus facile de débrancher une clé USB durant le fonctionnement d'un ordinateur que le disque dur qu'il y a à l'intérieur : ce genre d'erreur est fréquent, personne n'est à l'abri. Il arrive donc que le système de fichier de *persistance* devienne inconsistant. En général, les parties inconsistantes sont rapidement réparées au démarrage suivant. Cependant, il arrive que la clé USB devienne inutilisable en cas d'inconsistance grave.

D'autre part, ces clés USB sont destinées à l'apprentissage des l'informatique sur système libre, pour des débutants. Or *tous les droits* en écriture peuvent être attribués à l'utilisateur débutant. Si on a l'idée d'effacer un fichier important pour le fonctionnement de l'ordinateur, c'est possible. Fort heureusement, le cœur du système est en lecture-seule ; quand on croit effacer un fichier, en réalité on écrit dans la persistance un code signifiant « ce fichier est effacé » ; le fichier est cependant toujours là dans le cœur du système,  ce qui permet la magie de la réinitialisation.

## Réinitialisation de la clé vive ##

En fait, pour réinitialiser la clé vive, il faut et il suffit d'effacer deux dossier dans la partition de persistance. Bien sûr, ce faisant on perd tout le travail et toutes les configurations effectuées par l'utilisateur depuis le début de l'histoire de la clé. C'est pourquoi les outils de maintenance sont organisés de façon *prudente*.

## Première étape prudente : garder la liste des paquets logiciels nouveaux ##

La clé vient avec des centaines de logiciels pré-installés, mais on peut en vouloir plus. Grâce aux paquets logiciels du système Debian, rien de plus facile ! les commandes ``apt`` permettent de le faire en toute liberté. Quand la persistance est effacée, c'est une bonne idée de conserver quelque part la liste de tous les paquets logiciels qu'on peut souhaiter réinstaller ensuite.

## Deuxième étape prudente : sauvegarder les données de l'utilisateur ##

Durant l'histoire d'une clé, la plupart des fichiers intéressants pour l'utilisateur se placent dans le répertoire ``/home/user`` ; on est donc invité à en faire une sauvegarde, totale ou sélective. L'interface des outils propose une vision en arbre de dossiers, qui permet de cocher ce qu'on veut conserver :

* tout conserver ... cocher simplement le dossier ``user``
* conserver par exemple les contenu du bureau, les téléchargements, et un projet situé dans le dossier ``/home/user/projets/projet1`` : ne *pas cocher* le dossier ``user``, cocher les dossiers ``user/Bureau``, ``user/Téléchargement``, ``user/projets/projet1`` ; notez bien que si on coche deux points dans l'arbre des fichiers, le point coché le plus proche de la racine peut passer *devant* l'autre, s'il se situe dans un sous-répertoire : c'est toujours la sauvegarde la plus large qui est choisie.

La sauvegarde se fait au format ``tar.gz``, aussi connue sous le nom ``tgz`` ; les bons gestionnaires d'archive savent tous interpréter ce format.

Si on tente un maintenance sur une clé neuve, il n'y a pas de données personnelles dans la persistance, donc cette étape est écourtée.

## Dernière étape : réinitialiser la persistance ##

Cette étape, comme toutes celles qui précèdent, peut être neutralisée, il suffit de choisir un échappement, possible à chaque moment. Comme la réinitialisation se fait avec perte de données, on doit confirmer deux fois qu'on veut bien la faire.

L'opération est rapide, et irréversible. Après réinitialisation, la clé USB est prête à fonctionner comme à son premier démarrage  : donc elle démarrera aussi lentement qu'elle l'a fait à a première utilisation, puisque de nombreuses initialisations doivent avoir lieu (un *premier* démarrage prend une à trois minutes de plus que les suivants).

# Lancement du système dans une machine virtuelle #

Tant qu'on reste dans la même application, on peut tester le fonctionnement de la clé restaurée dans une machine virtuelle, sur le même ordinateur, sans arrêter son fonctionnement. C'est possible si le processeur est équipé pour pouvoir supporter la virtualisation (ce n'est pas le cas de tous les processeurs).

Le paquet logiciel ``qemu-kvm`` est nécessaire pour pouvoir profiter de cette fonctionnalité. Il n'est pas installé par défaut dans la distribution Freeduc, mais on peut l'y ajouter. Notez bien que le fonctionnement en machine virtuelle est l'une des seules façons permettant d'obtenir des copies d'écran durant les phases de démarrage et d'arrêt du système, quand le système lui-même serait bien incapable de gérer l'opération de copie d'écran.
