© 2019 Georges Khaznadar <georgesk@debian.org>

Ce programme est libre, il est distribué sous la licence GPL-3+.

Live-Clone a été développé pour permettre aux 
[clés vives Freeduc](https://usb.freeduc.org/jbart.html) de disposer
d'un système d'auto-clonage, ce qui facilite leur sécurisation et
leur dissémination.
