<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>BackupDialog</name>
    <message>
        <location filename="../ui_selectBackupData.py" line="42"/>
        <source>Select backup data</source>
        <translation type="obsolete">Sélection des données personnelles à enregistrer</translation>
    </message>
    <message>
        <location filename="../ui_selectBackupData.py" line="43"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Please check the tree of data which you want to backup. If you check the topmost node &amp;quot;user&amp;quot;, all and every personal data is chosen, including Mozilla cache and desktop preferences. You may prefer to check subtrees. The subtrees are taken in account only when no node is checked above.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Veuillez cocher le ou les arbres de données que vous voulez sauvegarder. Si on coche le nœud de niveau supérieur « user », toutes les données personnelles sont enregistrées, y compris le cache de Mozilla Firefox et les préférences du bureau. Vous pouvez préférer seulement sélectionner des sous-arbres. Les sous- arbres sont pris en compte si aucun nœud de niveau supérieur n'est coché.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
</context>
<context>
    <name>CloneMonitor</name>
    <message>
        <location filename="../monitor.py" line="212"/>
        <source>Cloning is over</source>
        <translation>Le clonage est terminé</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="213"/>
        <source>Cloning</source>
        <translation>Clonage</translation>
    </message>
</context>
<context>
    <name>CloneWizard</name>
    <message>
        <location filename="../wizards.py" line="364"/>
        <source>Clone a Freeduc system to USB drives</source>
        <translation>Cloner un système Freeduc dans des clés USB</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="366"/>
        <source>Select the target disks</source>
        <translation>Sélection des disques-cibles</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="367"/>
        <source>Please check the disks where you want to clone the system. Warning: all data exiting on those disks will be erased, you can still Escape from this process.</source>
        <translation>Veuillez cocher les disques où vous voulez cloner le système. Attention : toutes les données de ces disques seront effacées. Vous pouvez encore vous échapper de ce processus.</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="436"/>
        <source>Source of the core system</source>
        <translation>Source du cœur du système</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="438"/>
        <source>The program is running from a Freeduc GNU-Linux system</source>
        <translation>Le programme fonctionne dans un système GNU Linux Freeduc</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="440"/>
        <source>The program is running from a plain GNU-Linux system</source>
        <translation>Le programme fonctionne dans un système GNU Linux ordinaire</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="444"/>
        <source>Choose an image to clone</source>
        <translation>Choisir une image à cloner</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="444"/>
        <source>ISO Images (*.iso);;All files (*)</source>
        <translation>Images ISO (*.iso);;Tous fichiers (*)</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="497"/>
        <source>Cloning to {}</source>
        <translation>Clonage vers {}</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="419"/>
        <source>Size of a VFAT partition (GB):</source>
        <translation>Taille d'une partition VFAT (Go) :</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../ui_about.py" line="37"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../ui_about.py" line="35"/>
        <source>&amp;lt;!DOCTYPE HTML PUBLIC &amp;quot;-//W3C//DTD HTML 4.0//EN&amp;quot; &amp;quot;http://www.w3.org/TR/REC-html40/strict.dtd&amp;quot;&amp;gt;
&amp;lt;html&amp;gt;&amp;lt;head&amp;gt;&amp;lt;meta name=&amp;quot;qrichtext&amp;quot; content=&amp;quot;1&amp;quot; /&amp;gt;&amp;lt;style type=&amp;quot;text/css&amp;quot;&amp;gt;
p, li { white-space: pre-wrap; }
&amp;lt;/style&amp;gt;&amp;lt;/head&amp;gt;&amp;lt;body style=&amp;quot; font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;&amp;quot;&amp;gt;
&amp;lt;p style=&amp;quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&amp;quot;&amp;gt;© 2019 Georges Khaznadar &amp;lt;georgesk@debian.org&amp;gt;&amp;lt;/p&amp;gt;
&amp;lt;p style=&amp;quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&amp;quot;&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;/p&amp;gt;
&amp;lt;p style=&amp;quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&amp;quot;&amp;gt;This program is free, it is licensed under GPL-3+.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation type="obsolete">&amp;lt;!DOCTYPE HTML PUBLIC &amp;quot;-//W3C//DTD HTML 4.0//EN&amp;quot; &amp;quot;http://www.w3.org/TR/REC-html40/strict.dtd&amp;quot;&amp;gt;
&amp;lt;html&amp;gt;&amp;lt;head&amp;gt;&amp;lt;meta name=&amp;quot;qrichtext&amp;quot; content=&amp;quot;1&amp;quot; /&amp;gt;&amp;lt;style type=&amp;quot;text/css&amp;quot;&amp;gt;
p, li { white-space: pre-wrap; }
&amp;lt;/style&amp;gt;&amp;lt;/head&amp;gt;&amp;lt;body style=&amp;quot; font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;&amp;quot;&amp;gt;
&amp;lt;p style=&amp;quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&amp;quot;&amp;gt;© 2019 Georges Khaznadar &amp;lt;georgesk@debian.org&amp;gt;&amp;lt;/p&amp;gt;
&amp;lt;p style=&amp;quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&amp;quot;&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;/p&amp;gt;
&amp;lt;p style=&amp;quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&amp;quot;&amp;gt;Ce programme est libre, il est régi par la licence GPL-3+.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
</context>
<context>
    <name>DiskinstallWizard</name>
    <message>
        <location filename="../wizards.py" line="521"/>
        <source>Clone a Freeduc system to the hard disk</source>
        <translation>Clone un système Freeduc sur le disque dur</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="540"/>
        <source>Cloning to {}</source>
        <translation>Clonage vers {}</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="552"/>
        <source>DANGER ZONE</source>
        <translation>ZONE DANGEREUSE</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="553"/>
        <source>Your USB disk is /dev/{usb}, its image would be written on the hard disk /dev/{hard}. Are you really sure that you want to erase and loose any data available on the hard disk /dev/{hard}?</source>
        <translation>Votre disque USB est /dev/{usb}, son image devrait être gravée sur le disque dur /dev/{hard}. Êtes-vous vraiment sur.e de vouloir effacer et perdre toutes les données qui sont sur le disque dur /dev/{hard}?</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="555"/>
        <source>Yes, I am sure that I want to erase the hard disk.</source>
        <translation>Oui, je suis sûr.e, Je veux effacer le disque dur.</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="556"/>
        <source>Yes again, I am completely sure!</source>
        <translation>Encore oui, j'en suis complètement sûr.e !</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="528"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="528"/>
        <source>You are not running this application from a live USB key. Aborting the clone action.</source>
        <translation>Vous n'avez pas lancé l'application depuis une clé USB vive. Annulation du clonage.</translation>
    </message>
</context>
<context>
    <name>HardCloneMonitor</name>
    <message>
        <location filename="../monitor.py" line="264"/>
        <source>Cloning is over</source>
        <translation type="obsolete">Le clonage est terminé</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="265"/>
        <source>Cloning</source>
        <translation type="obsolete">Clonage</translation>
    </message>
</context>
<context>
    <name>HardDiskMain</name>
    <message>
        <location filename="../hdmain.py" line="62"/>
        <source>Device</source>
        <translation>Périphérique</translation>
    </message>
    <message>
        <location filename="../hdmain.py" line="62"/>
        <source>Partitions</source>
        <translation>Partitions</translation>
    </message>
    <message>
        <location filename="../hdmain.py" line="62"/>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <location filename="../hdmain.py" line="98"/>
        <source>HelpDiskTab.md</source>
        <translation>HelpDiskTab.fr.md</translation>
    </message>
    <message>
        <location filename="../hdmain.py" line="105"/>
        <source>HelpAbout.md</source>
        <translation>lang/HelpAbout.fr.md</translation>
    </message>
    <message>
        <location filename="../hdmain.py" line="130"/>
        <source>Clone ready ... Tab #{0}</source>
        <translation>Clonage terminé ... Tab n°{0}</translation>
    </message>
    <message>
        <location filename="../hdmain.py" line="155"/>
        <source>Tab closed.</source>
        <translation>Tab refermé.</translation>
    </message>
    <message>
        <location filename="../hdmain.py" line="238"/>
        <source>{} ... Tab #{} {}</source>
        <translation>{} ... Tab n° {} {}</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui_diskinstall.py" line="128"/>
        <source>Freeduc Clone</source>
        <translation>Freeduc Clone</translation>
    </message>
    <message>
        <location filename="../ui_live_clone.py" line="163"/>
        <source>CLONING LIVE STICKS</source>
        <translation>CLONER DES CLÉS USB</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="101"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&amp;lt;!DOCTYPE HTML PUBLIC &amp;quot;-//W3C//DTD HTML 4.0//EN&amp;quot; &amp;quot;http://www.w3.org/TR/REC-html40/strict.dtd&amp;quot;&amp;gt;
&amp;lt;html&amp;gt;&amp;lt;head&amp;gt;&amp;lt;meta name=&amp;quot;qrichtext&amp;quot; content=&amp;quot;1&amp;quot; /&amp;gt;&amp;lt;style type=&amp;quot;text/css&amp;quot;&amp;gt;
p, li { white-space: pre-wrap; }
&amp;lt;/style&amp;gt;&amp;lt;/head&amp;gt;&amp;lt;body style=&amp;quot; font-family:'Sans'; font-size:16pt; font-weight:400; font-style:normal;&amp;quot;&amp;gt;
&amp;lt;p style=&amp;quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&amp;quot;&amp;gt;&amp;lt;br /&amp;gt;&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="144"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="145"/>
        <source>Tab 2</source>
        <translation>Tab 2</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="118"/>
        <source>&amp;File</source>
        <translation type="obsolete">&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="119"/>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Aide</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="190"/>
        <source>&amp;Quit</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="191"/>
        <source>&amp;About</source>
        <translation type="obsolete">À &amp;Propos</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="120"/>
        <source>Tools</source>
        <translation type="obsolete">Outils</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="192"/>
        <source>Erase persistence data</source>
        <translation type="obsolete">Effacement des données persistantes</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="193"/>
        <source>When something goes really wrong, erasing persistence data allows one to return to the initial setup</source>
        <translation type="obsolete">Quand ça part vraiment mal, on peut revenir à la configuration initiale en effaçant les données persistantes</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="194"/>
        <source>Backup personal data</source>
        <translation type="obsolete">Enregistrer les données personnelles</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="195"/>
        <source>When persistence data are erase, personal data disappear: you can backup them in advance</source>
        <translation type="obsolete">Quand les données persistantes sont effacées, les données personnelles aussi : on peut les enregistrer par avance</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="196"/>
        <source>List additional packages</source>
        <translation type="obsolete">Lister les paquets ajoutés</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="197"/>
        <source>When the persistence area is erased all newer package are erased too. One might know their list.</source>
        <translation type="obsolete">Quand on efface la zone de persistance, tous les nouveaux paquets sont effacés aussi. On peut vouloir en connaître la liste.</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="198"/>
        <source>Run in a virtual machine</source>
        <translation type="obsolete">Démarrer dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="169"/>
        <source>Clone the system core to a USB disk</source>
        <translation type="obsolete">Cloner le cœur du système vers un disque USB</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="142"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="171"/>
        <source>Save personal data, Erase persistence</source>
        <translation type="obsolete">Enregistrer les données personnelles, Effacer la persistance</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="173"/>
        <source>Launch in a virtual machine</source>
        <translation type="obsolete">Démarrer dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="177"/>
        <source>Quit the application</source>
        <translation type="obsolete">Quitter l'application</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="130"/>
        <source>Help(F1)</source>
        <translation>Aide (F1)</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="132"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../ui_live_clone.py" line="150"/>
        <source>Save personal data, Erase persistence (Alt-S)</source>
        <translation>Enregistrer les données personnelles, Effacer la persistance (Alt-S)</translation>
    </message>
    <message>
        <location filename="../ui_live_clone.py" line="152"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../ui_live_clone.py" line="154"/>
        <source>Launch in a virtual machine (Alt-R)</source>
        <translation>Démarrer dans une machine virtuelle (Alt-R)</translation>
    </message>
    <message>
        <location filename="../ui_live_clone.py" line="156"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="138"/>
        <source>Clone the system core to a USB disk (Alt-C)</source>
        <translation>Cloner le cœur du système vers un disque USB (Alt-C)</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="136"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="141"/>
        <source>Quit the application (Ctrl-Q)</source>
        <translation>Quitter l'application (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="143"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="385"/>
        <source>Installation on the hard disk (danger zone)</source>
        <translation>Installation sur le disque dur (zone de danger)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="398"/>
        <source>Installation on the hard disk is only possible from a running live USB disk!</source>
        <translation type="obsolete">L'installation sur le disque dur n'est possible que depuis une clé USB vive en fonctionnement !</translation>
    </message>
    <message>
        <location filename="../ui_diskinstall.py" line="139"/>
        <source>CLONING TO HARD DISK</source>
        <translation>CLONAGE VERS LE DISQUE DUR</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="383"/>
        <source>Simulated clone on a temporary 16 GB volume</source>
        <translation type="obsolete">Clonage simulé, vers un volume de 16 Go temporaire</translation>
    </message>
</context>
<context>
    <name>Monitor</name>
    <message>
        <location filename="../monitor.py" line="73"/>
        <source>stdout</source>
        <translation>Sortie standard</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="80"/>
        <source>stderr</source>
        <translation>Sorties standard des « erreurs »</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="78"/>
        <source>Cloning is over</source>
        <translation type="obsolete">Le clonage est terminé</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="46"/>
        <source>Do you want to save the logs?</source>
        <translation type="obsolete">Voulez-vous enregistrer les traces d'exécution ?</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="46"/>
        <source>name of the file for the logs</source>
        <translation type="obsolete">Nom du fichier pour les traces</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="110"/>
        <source>Save the messages?</source>
        <translation>Enregistrer les messages ?</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="110"/>
        <source>Log files (*.log);;All files (*)</source>
        <translation>Fichers de journalisation (*.log);;Tous fichiers (*)</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="46"/>
        <source>Close this monitor</source>
        <translation>Fermer ce moniteur</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="49"/>
        <source>Working</source>
        <translation>Travail</translation>
    </message>
</context>
<context>
    <name>MyMain</name>
    <message>
        <location filename="../__init__.py" line="208"/>
        <source>Tab closed.</source>
        <translation>Tab refermé.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="175"/>
        <source>HelpTab.md</source>
        <translation>lang/HelpTab.fr.md</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="183"/>
        <source>Clone ready ... Tab #{0}</source>
        <translation>Clonage terminé ... Tab n°{0}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="264"/>
        <source>Clone to {}</source>
        <translation>Cloner vers {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="347"/>
        <source>Device</source>
        <translation>Périphérique</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="347"/>
        <source>Partitions</source>
        <translation>Partitions</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="468"/>
        <source>Source</source>
        <translation type="obsolete">Source</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="468"/>
        <source>Actions</source>
        <translation type="obsolete">Actions</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="347"/>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="419"/>
        <source>Own running system</source>
        <translation type="obsolete">Le système en cours</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="504"/>
        <source>Choose an image to clone to {}</source>
        <translation type="obsolete">Choisir une image à cloner vers {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="190"/>
        <source>ISO Images (*.iso);;All files (*)</source>
        <translation type="obsolete">Images ISO (*.iso);;Tous fichiers (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="513"/>
        <source>Cloning ... Tab #{}</source>
        <translation type="obsolete">Clonage ... Tab n°{}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="354"/>
        <source>Cloning to {}</source>
        <translation type="obsolete">Clonage vers {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="168"/>
        <source>HelpAbout.md</source>
        <translation>lang/HelpAbout.fr.md</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="325"/>
        <source>Current USB disks: {}</source>
        <translation>Disques USB connectés : {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="327"/>
        <source>No USB stick. Please plug a USB flash disk.</source>
        <translation>Pas de disque USB connecté. Veuillez en connecter un.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="357"/>
        <source>Erase persistence data</source>
        <translation type="obsolete">Effacement des données persistantes</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="358"/>
        <source>If you go forward, all data in the persistence partition(s) will be lost. If unsure, cancel the operation. The list of erasable persistence partitions is below.</source>
        <translation type="obsolete">Si vous persistez, toutes les données dans la ou les partitions de persistace seront effacées. En cas de doute, Échappez à cette opération. La liste des partitions effaçables est ci-dessous.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="365"/>
        <source>Select the partitions to clear</source>
        <translation type="obsolete">Sélection des partitions à effacer</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="366"/>
        <source>Be careful. The selected partitions will be cleared when you finish.</source>
        <translation type="obsolete">Attention, les partitions sélectionnées seront effacées quand vous demanderez à finir.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="276"/>
        <source>No selected USB disk</source>
        <translation type="obsolete">Pas de disque USB choisi</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="276"/>
        <source>Please select at least one cell in the disks table.</source>
        <translation type="obsolete">Veuillez sélectionner une case au moins dans le tableau des disques.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="297"/>
        <source>Get a list of additional packages</source>
        <translation type="obsolete">Réupération d'une liste des paquets ajoutés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="325"/>
        <source>Select one disk</source>
        <translation type="obsolete">Sélectionner un disque</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="300"/>
        <source>Which disk do you want to check for packages installed in the persistence zone?</source>
        <translation type="obsolete">Sur quel disque voulez-vous vérifier la liste des paquets installés dans la zone de persistance ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="323"/>
        <source>Run a disk in a virtual machine</source>
        <translation type="obsolete">Démarre un disque dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="326"/>
        <source>Which disk do you want to launch in a virtual machine?</source>
        <translation type="obsolete">Quel disque voulez-vous démarrer dans une machine virtuelle ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="169"/>
        <source>ADDITIONAL PACKAGES
===================
</source>
        <translation type="obsolete">LISTE DES PAQUETS AJOUTÉS
=========================
</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="186"/>
        <source>Save Packages File</source>
        <translation type="obsolete">Enregistrement d'un fichier de paquets</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="186"/>
        <source>Markdow files (*.md);;Plain text files (*.txt);;Any file (*)</source>
        <translation type="obsolete">Fichiers Markdow (*.md);;Fichiers texte (*.txt);;Tout fichier (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="224"/>
        <source>Backup personal data</source>
        <translation type="obsolete">Enregistrer les données personnelles</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="227"/>
        <source>Which is the disk containing personal data to backup?</source>
        <translation type="obsolete">Quel est le disque qui contient les données personnelles à enregistrer ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="157"/>
        <source>List of additional packages in {}</source>
        <translation type="obsolete">Liste des paquets ajoutés dans {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="142"/>
        <source>Clone a Freeduc system to USB drives</source>
        <translation type="obsolete">Cloner un système Freeduc dans des clés USB</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="144"/>
        <source>Select the target disks</source>
        <translation type="obsolete">Sélection des disques-cibles</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="145"/>
        <source>Please check the disks where you want to clone the system. Warning: all data exiting on those disks will be erased, you can still Escape from this process.</source>
        <translation type="obsolete">Veuillez cocher les disques où vous voulez cloner le système. Attention : toutes les données de ces disques seront effacées. Vous pouvez encore vous échapper de ce processus.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="182"/>
        <source>Source of the core system</source>
        <translation type="obsolete">Source du cœur du système</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="184"/>
        <source>The program is running from a Freeduc GNU-Linux system</source>
        <translation type="obsolete">Le programme fonctionne dans un système GNU Linux Freeduc</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="186"/>
        <source>The program is running from a plain GNU-Linux system</source>
        <translation type="obsolete">Le programme fonctionne dans un système GNU Linux ordinaire</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="190"/>
        <source>Choose an image to clone</source>
        <translation type="obsolete">Choisir une image à cloner</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="133"/>
        <source>Repair and/or manage a Freeduc USB stick</source>
        <translation type="obsolete">Réparer et/ou gérer une clé USB Freeduc</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="136"/>
        <source>Which disk do you want to repair or to save?</source>
        <translation type="obsolete">Quelle clé voulez-vous réparer ou sauvegarder ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="158"/>
        <source>Here is the list of packages which you can restore later. You can edit the text before saving it.</source>
        <translation type="obsolete">Voici la liste des paquets que vous pourriez restaurer plus tard. Il est possible d'éditer ce texte avant de l'enregistrer.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="168"/>
        <source>There are no new packages installed in the persistence partition</source>
        <translation type="obsolete">Il n'y a pas de nouveaux paquets installés sur la partition de persistance</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="301"/>
        <source>Select personal data to save</source>
        <translation type="obsolete">Sélectionnez les données personnelles à enregistrer</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="206"/>
        <source>Please check the tree of data which you want to backup. If you check the topmost node &quot;user&quot;, all and every personal data is chosen, including Mozilla cache and desktop preferences. You may prefer to check subtrees. The subtrees are taken in account only when no node is checked above.</source>
        <translation type="obsolete">Veuillez cocher l'arbre de données que vous voulez sauvegarder. Si vous cochez le nœud supérieur « user », toutes les données personnelles sont sélectionnées, y compris le cache de Mozilla Firefox, les préférences de bureau et la (grosse) configuration de l'émulateur Windows. Vous pourriez préférer cocher des sous-arbres. Les sous-arbres sont pris en compte pour autant qu'aucun nœud ne soit coché au-dessus d'eux.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="259"/>
        <source>TAR-GZIP archives (*.tgz,*.tar.gz);;Any file (*)</source>
        <translation type="obsolete">Achives TAR-GZIP (*.tgz,*.tar.gz);;Tout fichier (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="273"/>
        <source>Save personal persistence data to {}</source>
        <translation type="obsolete">Enregistrer les données personnelles dans {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="275"/>
        <source>Save to {}</source>
        <translation type="obsolete">Enregistrer dans {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="285"/>
        <source>save-data</source>
        <translation type="obsolete">donnees</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="285"/>
        <source>Close the log of the archive</source>
        <translation type="obsolete">Fermer le journal d'archivage</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="279"/>
        <source>{} ... Tab #{}</source>
        <translation type="obsolete">{} ... Tab n° {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="231"/>
        <source>Nothing is available</source>
        <translation type="obsolete">Rien trouvé</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="236"/>
        <source>No personal data in the persistence partition</source>
        <translation type="obsolete">Il n'y a pas de données personnelles dans la partition de persistance</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="302"/>
        <source>Now, you have carefully backuped sensistive data, you may erase the persistence; however, you can still cancel it.</source>
        <translation type="obsolete">Maintenant, vous avez soigneusement sauvegardé les données sensibles, vous pouvez effacer la persistance ; mais vous pouvez encore y échapper.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="312"/>
        <source>Persistence erased</source>
        <translation type="obsolete">Données de persistance effacées</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="312"/>
        <source>Persistence data have been erased from {}</source>
        <translation type="obsolete">Les données de persistance de {} ont été effacées</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="346"/>
        <source>Yes, I am sure that I want to erase the persistence.</source>
        <translation type="obsolete">Oui, je suis sûr.e de vouloir effcer la persistence.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="347"/>
        <source>Yes again, I am completely sure!</source>
        <translation type="obsolete">Encore oui, j'en suis complètement sûr.e !</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="259"/>
        <source>Save personal data archive</source>
        <translation type="obsolete">Enregistrer l'archive des données personnelles</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="331"/>
        <source>Format the partition {}</source>
        <translation type="obsolete">Formater la partition {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="287"/>
        <source>{} ... Tab #{} {}</source>
        <translation>{} ... Tab n° {} {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="347"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>PackageEditor</name>
    <message>
        <location filename="../ui_packageEditor.py" line="57"/>
        <source>Additional packages</source>
        <translation>Paquets ajoutés</translation>
    </message>
    <message>
        <location filename="../ui_packageEditor.py" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This editor contains the packages installed in the persistence zone. They will be lost if one erases the persistence data. You can edit this text and save it.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Cet éditeur liste les paquets installés dans la zone de persistance. Ils seront perdus si on efface les données de persistance. Vous pouvez éditer ce texte et l'enregistrer.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../ui_packageEditor.py" line="59"/>
        <source>Cancel</source>
        <translation>Échappement</translation>
    </message>
    <message>
        <location filename="../ui_packageEditor.py" line="60"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
</context>
<context>
    <name>RunWizard</name>
    <message>
        <location filename="../wizards.py" line="579"/>
        <source>Run a disk in a virtual machine</source>
        <translation>Démarre un disque dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="581"/>
        <source>Select one disk</source>
        <translation>Sélectionner un disque</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="582"/>
        <source>Which disk do you want to launch in a virtual machine?</source>
        <translation>Quel disque voulez-vous démarrer dans une machine virtuelle ?</translation>
    </message>
</context>
<context>
    <name>StickMaker</name>
    <message>
        <location filename="../makeLiveStick.py" line="117"/>
        <source>[At {0}]    {1}</source>
        <translation>[À {0}]    {1}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="184"/>
        <source>You must be root to launch {}</source>
        <translation>Il faut être root pour lancer {}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="60"/>
        <source>clone a Freeduc-Jbart distribution</source>
        <translation type="obsolete">cloner une distribution Freeduc-Jbart</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="194"/>
        <source>do&apos;nt use the file rw.tar.gz to seed the persistence</source>
        <translation>ne pas utiliser le fichier rw.tar.gz pour initialiser la partition de persistence</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="62"/>
        <source>source of the distribution (required); the keyword OWN means that we are cloning from Freeduc-Jbart</source>
        <translation type="obsolete">source de la distribution (obligatoire) ; le mot-clé OWN signifie qu'on clone depuis une clé USB Freeduc-Jbart</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="197"/>
        <source>the targetted USB stick</source>
        <translation type="obsolete">la clé USB cible</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="89"/>
        <source>Unmounting {}</source>
        <translation type="obsolete">Démontage de {}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="264"/>
        <source>Copying {0} to {1} ...</source>
        <translation>Copie de {0} vers {1} ...</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="113"/>
        <source>Forget the partition {}3, to create it again</source>
        <translation type="obsolete">Suppression de la partition {}3, pour la créer à nouveau</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="120"/>
        <source>Create the partition {}3 to support persistence</source>
        <translation type="obsolete">Création de la partition {}3 pour supporter la persistance</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="339"/>
        <source>This partition is adjusted to use all the remaining space on {}</source>
        <translation>On ajuste cette partition pour utiliser la place restante sur {}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="156"/>
        <source>Create the file persistence.conf on the third partition</source>
        <translation type="obsolete">Création du fichier persistence.conf sur la troisième partition</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="163"/>
        <source>Pre-seed the third partition with rw.tar.gz</source>
        <translation type="obsolete">Initialisation de la troisième partition à l'aide de rw.tar.gz</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="431"/>
        <source>Ready. Total time: {}</source>
        <translation>Terminé. Durée totale : {}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="191"/>
        <source>clone a Debian-Live distribution</source>
        <translation>cloner une distribution Debian-Live</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="195"/>
        <source>source of the distribution (required); the keyword OWN means that we are cloning from a Debian-Live disk</source>
        <translation>source de la distribution (obligatoire) ; le mot-clé OWN signifie qu'on clone depuis une clé USB Debian-Live</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="122"/>
        <source>Create the partition {}3 for VFAT data, using 4GiB</source>
        <translation type="obsolete">Création de la partition {}3, format VFAT, taille 4Gio</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="276"/>
        <source>Create the partition {}4 to support persistence</source>
        <translation type="obsolete">Création de la partition {}4 pour supporter la persistance</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="347"/>
        <source>Create the file persistence.conf on the fourth partition</source>
        <translation type="obsolete">Création du fichier persistence.conf sur la quatrième partition</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="358"/>
        <source>Pre-seed the fourth partition with rw.tar.gz</source>
        <translation type="obsolete">Initialisation de la quatrième partition à l'aide de rw.tar.gz</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="258"/>
        <source>Delete the partition {}{}, to create it again</source>
        <translation>Suppression de la partition {}{}, pour la créer à nouveau</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="192"/>
        <source>size of the VFAT partition, in GB</source>
        <translation>Taille de la partition VFAT, en Go</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="242"/>
        <source>Create the partition {}3 for VFAT data, using {}GiB</source>
        <translation type="obsolete">Création de la partition {}3, format VFAT, taille {}Gio</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="243"/>
        <source>The disk is too small ({} GiB) to contain the ISO image ({} GiB) plus at least 1 GB for the persistence</source>
        <translation>Le disque est manque de capacité ({} Go) pour contenir l'image ISO ({} Go) plus au moins 1 Go pour la persistance</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="248"/>
        <source>The size of the VFAT partition has been shrinked down to {} GB</source>
        <translation>La taille de la partition VFAT a été diminuée à {} Go</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="193"/>
        <source>size of the ISO image, in GB (required only when cloning from a block device)</source>
        <translation>taille de l'image ISO, en Go (nécessaire seulement quand on clone depuis un périphérique de bloc)</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="201"/>
        <source>Test command: {0}</source>
        <translation type="obsolete">Commande de test : {0}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="196"/>
        <source>simulate the cloning onto a loop-mounted temporary file</source>
        <translation>simulation d'un clonage sur un fichier temporaire monté en boucle</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="103"/>
        <source>Eject and reload the disk {}, to let the kernel understand its new structure</source>
        <translation>Éjection et remontage du disque {}, afin que le noyau reconnaisse sa structure</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="240"/>
        <source>Create the partition {}p3 for VFAT data, using {}GiB</source>
        <translation type="obsolete">Création de la partition {}p3 pour les données VFAT, avec {} Go</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="274"/>
        <source>Create the partition {}p4 to support persistence</source>
        <translation type="obsolete">Création de la partition {}p4 pour supporter la persistance</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="92"/>
        <source>Aiming to run the command {cmd}</source>
        <translation>Sur le point de lancer la commande {cmd}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="282"/>
        <source>Create the extended partition {}p3</source>
        <translation>Création de la partition étendue {}p3</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="284"/>
        <source>Create the extended partition {}3</source>
        <translation>Création de la partition étendue {}3</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="302"/>
        <source>Create Extended Partition</source>
        <translation>Création de la partition étendue</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="307"/>
        <source>Create the partition {}p5 for VFAT data, using {}GiB</source>
        <translation>Création de la partition {}p5 pour les données VFAT, avec {} Go</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="309"/>
        <source>Create the partition {}5 for VFAT data, using {}GiB</source>
        <translation>Création de la partition {}5, format VFAT, taille {}Gio</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="329"/>
        <source>Create Vfat</source>
        <translation>Création de la partition Vfat</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="336"/>
        <source>Create the partition {}p6 to support persistence</source>
        <translation>Création de la partition {}p6 pour supporter la persistance</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="338"/>
        <source>Create the partition {}6 to support persistence</source>
        <translation>Création de la partition {}6 pour supporter la persistance</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="361"/>
        <source>Create Ext4</source>
        <translation>Création de la partition Ext4</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="386"/>
        <source>Format vfat and ext4 partitions</source>
        <translation>Format des partitions Vfat et Ext4</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="400"/>
        <source>Create the file persistence.conf on the last partition</source>
        <translation>Création du fichier persistence.conf sur la dernière partition</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="411"/>
        <source>Pre-seed the last partition with rw.tar.gz</source>
        <translation>Pré-ensemencenment de la dernière partition avec le fichier rw.tar.gz</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="197"/>
        <source>the targetted USB stick, or the name of the file to create in case of a simulation</source>
        <translation>La clé USB ciblée, ou le nom d'un fichier à créer dans le cas d'une simulation</translation>
    </message>
</context>
<context>
    <name>Tool</name>
    <message>
        <location filename="../tools.py" line="301"/>
        <source>Cannot erase persistence data</source>
        <translation>Impossible d'effacer les données de persistance</translation>
    </message>
    <message>
        <location filename="../tools.py" line="301"/>
        <source>One needs root priviledge to erase persistence data</source>
        <translation>Il faut avoir les privilèges de super-utilisateur pour effacer les données de persistence</translation>
    </message>
    <message>
        <location filename="../tools.py" line="314"/>
        <source>Persistence erased</source>
        <translation>Données de persistance effacées</translation>
    </message>
    <message>
        <location filename="../tools.py" line="314"/>
        <source>Persistence data have been erased from {}</source>
        <translation>Les données de persistance de {} ont été effacées</translation>
    </message>
    <message>
        <location filename="../tools.py" line="265"/>
        <source>Cancel</source>
        <translation type="obsolete">Échappement</translation>
    </message>
</context>
<context>
    <name>ToolWizard</name>
    <message>
        <location filename="../wizards.py" line="104"/>
        <source>Repair and/or manage a Freeduc USB stick</source>
        <translation>Réparer et/ou gérer une clé USB Freeduc</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="106"/>
        <source>Select one disk</source>
        <translation>Sélectionner un disque</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="107"/>
        <source>Which disk do you want to repair or to save?</source>
        <translation>Quelle clé voulez-vous réparer ou sauvegarder ?</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="129"/>
        <source>List of additional packages in {}</source>
        <translation>Liste des paquets ajoutés dans {}</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="130"/>
        <source>Here is the list of packages which you can restore later. You can edit the text before saving it.</source>
        <translation>Voici la liste des paquets que vous pourriez restaurer plus tard. Il est possible d'éditer ce texte avant de l'enregistrer.</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="153"/>
        <source>There are no new packages installed in the persistence partition</source>
        <translation>Il n'y a pas de nouveaux paquets installés sur la partition de persistance</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="155"/>
        <source>ADDITIONAL PACKAGES
===================
</source>
        <translation>LISTE DES PAQUETS AJOUTÉS
=========================
</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="172"/>
        <source>Save Packages File</source>
        <translation>Enregistrement d'un fichier de paquets</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="172"/>
        <source>Markdow files (*.md);;Plain text files (*.txt);;Any file (*)</source>
        <translation>Fichiers Markdow (*.md);;Fichiers texte (*.txt);;Tout fichier (*)</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="287"/>
        <source>Select personal data to save</source>
        <translation>Sélectionnez les données personnelles à enregistrer</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="192"/>
        <source>Please check the tree of data which you want to backup. If you check the topmost node &quot;user&quot;, all and every personal data is chosen, including Mozilla cache and desktop preferences. You may prefer to check subtrees. The subtrees are taken in account only when no node is checked above.</source>
        <translation>Veuillez cocher l'arbre de données que vous voulez sauvegarder. Si vous cochez le nœud supérieur « user », toutes les données personnelles sont sélectionnées, y compris le cache de Mozilla Firefox, les préférences de bureau et la (grosse) configuration de l'émulateur Windows. Vous pourriez préférer cocher des sous-arbres. Les sous-arbres sont pris en compte pour autant qu'aucun nœud ne soit coché au-dessus d'eux.</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="218"/>
        <source>Nothing is available</source>
        <translation>Rien trouvé</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="223"/>
        <source>No personal data in the persistence partition</source>
        <translation>Il n'y a pas de données personnelles dans la partition de persistance</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="245"/>
        <source>Save personal data archive</source>
        <translation>Enregistrer l'archive des données personnelles</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="245"/>
        <source>TAR-GZIP archives (*.tgz,*.tar.gz);;Any file (*)</source>
        <translation>Achives TAR-GZIP (*.tgz,*.tar.gz);;Tout fichier (*)</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="259"/>
        <source>Save personal persistence data to {}</source>
        <translation>Enregistrer les données personnelles dans {}</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="261"/>
        <source>Save to {}</source>
        <translation>Enregistrer dans {}</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="271"/>
        <source>save-data</source>
        <translation>donnees</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="271"/>
        <source>Close the log of the archive</source>
        <translation>Fermer le journal d'archivage</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="288"/>
        <source>Now, you have carefully backuped sensistive data, you may erase the persistence; however, you can still cancel it.</source>
        <translation>Maintenant, vous avez soigneusement sauvegardé les données sensibles, vous pouvez effacer la persistance ; mais vous pouvez encore y échapper.</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="308"/>
        <source>Persistence erased</source>
        <translation>Données de persistance effacées</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="308"/>
        <source>Persistence data have been erased from {}</source>
        <translation>Les données de persistance de {} ont été effacées</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="327"/>
        <source>Format the partition {}</source>
        <translation>Formater la partition {}</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="342"/>
        <source>Yes, I am sure that I want to erase the persistence.</source>
        <translation>Oui, je suis sûr.e de vouloir effcer la persistence.</translation>
    </message>
    <message>
        <location filename="../wizards.py" line="343"/>
        <source>Yes again, I am completely sure!</source>
        <translation>Encore oui, j'en suis complètement sûr.e !</translation>
    </message>
</context>
<context>
    <name>clone_jbart</name>
    <message>
        <location filename="../clone_jbart.py" line="61"/>
        <source>HelpTab.md</source>
        <translation type="obsolete">HelpTab.fr.md</translation>
    </message>
    <message>
        <location filename="../clone_jbart.py" line="74"/>
        <source>Clone ready ... Tab #{0}</source>
        <translation type="obsolete">Clonage terminé ... Tab n°{0}</translation>
    </message>
</context>
<context>
    <name>specialPage</name>
    <message>
        <location filename="../__init__.py" line="134"/>
        <source>Source of the core system</source>
        <translation type="obsolete">Source du cœur du système</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="136"/>
        <source>The program is running from a Freeduc GNU-Linux system</source>
        <translation type="obsolete">Le programme fonctionne dans un système GNU Linux Freeduc</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="138"/>
        <source>The program is running from a plain GNU-Linux system</source>
        <translation type="obsolete">Le programme fonctionne dans un système GNU Linux ordinaire</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="142"/>
        <source>Choose an image to clone</source>
        <translation type="obsolete">Choisir une image à cloner</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="142"/>
        <source>ISO Images (*.iso);;All files (*)</source>
        <translation type="obsolete">Images ISO (*.iso);;Tous fichiers (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="177"/>
        <source>Backup personal data</source>
        <translation type="obsolete">Enregistrer les données personnelles</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="332"/>
        <source>Select one disk</source>
        <translation type="obsolete">Sélectionner un disque</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="180"/>
        <source>Which is the disk containing personal data to backup?</source>
        <translation type="obsolete">Quel est le disque qui contient les données personnelles à enregistrer ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="207"/>
        <source>List of additional packages in {}</source>
        <translation type="obsolete">Liste des paquets ajoutés dans {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="210"/>
        <source>ADDITIONAL PACKAGES
===================
</source>
        <translation type="obsolete">LISTE DES PAQUETS AJOUTÉS
=========================
</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="226"/>
        <source>Save Packages File</source>
        <translation type="obsolete">Enregistrement d'un fichier de paquets</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="226"/>
        <source>Markdow files (*.md);;Plain text files (*.txt);;Any file (*)</source>
        <translation type="obsolete">Fichiers Markdow (*.md);;Fichiers texte (*.txt);;Tout fichier (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="283"/>
        <source>No selected USB disk</source>
        <translation type="obsolete">Pas de disque USB choisi</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="283"/>
        <source>Please select at least one cell in the disks table.</source>
        <translation type="obsolete">Veuillez sélectionner une case au moins dans le tableau des disques.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="304"/>
        <source>Get a list of additional packages</source>
        <translation type="obsolete">Réupération d'une liste des paquets ajoutés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="307"/>
        <source>Which disk do you want to check for packages installed in the persis?ence zone?</source>
        <translation type="obsolete">Sur quel disque voulez-vous vérifier la liste des paquets installés dans la zone de persistance ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="330"/>
        <source>Run a disk in a virtual machine</source>
        <translation type="obsolete">Démarre un disque dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="333"/>
        <source>Which disk do you want to launch in a virtual machine?</source>
        <translation type="obsolete">Quel disque voulez-vous démarrer dans une machine virtuelle ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="363"/>
        <source>Erase persistence data</source>
        <translation type="obsolete">Effacement des données persistantes</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="364"/>
        <source>If you go forward, all data in the persistence partition(s) will be lost. If unsure, cancel the operation. The list of erasable persistence partitions is below.</source>
        <translation type="obsolete">Si vous persistez, toutes les données dans la ou les partitions de persistace seront effacées. En cas de doute, Échappez à cette opération. La liste des partitions effaçables est ci-dessous.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="371"/>
        <source>Select the partitions to clear</source>
        <translation type="obsolete">Sélection des partitions à effacer</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="372"/>
        <source>Be careful. The selected partitions will be cleared when you finish.</source>
        <translation type="obsolete">Attention, les partitions sélectionnées seront effacées quand vous demanderez à finir.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="401"/>
        <source>HelpAbout.md</source>
        <translation type="obsolete">lang/HelpAbout.fr.md</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="416"/>
        <source>Clone ready ... Tab #{0}</source>
        <translation type="obsolete">Clonage terminé ... Tab n°{0}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="439"/>
        <source>Tab closed.</source>
        <translation type="obsolete">Tab refermé.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="476"/>
        <source>Clone to {}</source>
        <translation type="obsolete">Cloner vers {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="501"/>
        <source>Cloning ... Tab #{}</source>
        <translation type="obsolete">Clonage ... Tab n°{}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="526"/>
        <source>Current USB disks: {}</source>
        <translation type="obsolete">Disques USB connectés : {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="528"/>
        <source>No USB stick. Please plug a USB flash disk.</source>
        <translation type="obsolete">Pas de disque USB connecté. Veuillez en connecter un.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="547"/>
        <source>Device</source>
        <translation type="obsolete">Périphérique</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="547"/>
        <source>Partitions</source>
        <translation type="obsolete">Partitions</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="547"/>
        <source>Status</source>
        <translation type="obsolete">Statut</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="596"/>
        <source>Cloning to {}</source>
        <translation type="obsolete">Clonage vers {}</translation>
    </message>
</context>
<context>
    <name>uDisk</name>
    <message>
        <location filename="../usbDisk2.py" line="326"/>
        <source>Added partition %s</source>
        <translation>Partition ajoutée : %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="341"/>
        <source>Failed to mount the disk: %s</source>
        <translation>Échec au montage du disque : %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="390"/>
        <source>Disk not added: not a USB partition</source>
        <translation>Disque non ajouté : partition non-USB</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="347"/>
        <source>Disk not added: empty partition</source>
        <translation>Disque non ajouté : partition vide</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="377"/>
        <source>Already added disk: %s</source>
        <translation>Disque déjà ajouté : %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="379"/>
        <source>Added disk: %s</source>
        <translation>Disque ajouté : %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="414"/>
        <source>Change for the disk %s</source>
        <translation>Changement pour le disque %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="424"/>
        <source>Disk unplugged from the system: %s</source>
        <translation>Disque débranché du système : %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="509"/>
        <source>mount point</source>
        <translation>point de montage</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="510"/>
        <source>size</source>
        <translation>taille</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="511"/>
        <source>brand</source>
        <translation>marque</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="512"/>
        <source>disk model</source>
        <translation>modèle de disque</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="513"/>
        <source>serial number</source>
        <translation>numéro de série</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="516"/>
        <source>check</source>
        <translation>cocher</translation>
    </message>
</context>
</TS>
