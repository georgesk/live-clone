#!/usr/bin/python3

"""
sense the own system when cloning from a Freeduc drive
"""
from subprocess import run

def ownDevice(blockSize = 4194304):
    """
    Find the device path of the USB stick itself, when cloning in "own" mode
    
    @param blockSize the size of wanted blocks; defaults to 4194304 == 4M
    @return the path of the USB stick's device and its size in blocks unit
    """
    cmd="mount | grep /usr/lib/live/mount | grep /dev/sd | awk '{print $1}'| head -n 1"
    completed = run(cmd, shell=True, capture_output=True, encoding="utf-8")
    device = completed.stdout.strip()
    if device == "":
        raise Exception("No own device, are we running a non-Freeduc system?")
    cmd=f"LANG=C sfdisk -l {device} | head -n1|  awk '{{print $7}}'"
    completed = run(cmd, shell=True, capture_output=True, encoding="utf-8")
    sectors=int(completed.stdout.strip())
    count = 1 + sectors*512//blockSize # convert 512 B sectors to 4 MiB blocks
    return device, count
