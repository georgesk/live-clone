<style type="text/css">
h1 {
	color: darkred;
}

h2 {
	color: red;
}

h3 {
	color: blue;
}

p, ul, ol {
    margin-left: 30px;
    margin-right: 20px;
}

code, tt {
	font-family: Courier, fixed;
	font-weight: bold;
	color: brown;
}
</style>

# La barre d'outils ![barre d'outils](./img/toolbar1.fr.png) #

* ![Bouton d'aide](./img/help.png)Le premier bouton à gauche de la barre d'outil fait apparaître une fenêtre « à propos » de cette application, et remet le présent document sur le dessus.
* L'application centrale, c'est le clonage de la clé USB Freeduc vers le disque dur, ![Bouton de clonage](./img/jumping-gnu-48x48.png), qu'on peut lancer avec le bouton ou à l'aide du titre juste à côté.
* Le dernier bouton, tout à droite, permet de quitter l'application. ![Bouton pour quitter](./img/application-exit.png) On peut quitter même quand une commande est en cours et que les moniteurs en affichent les détails.

# Le déroulement d'un clonage #

Dans l'afficheur juste sous la barre d'outils, on peut voir des détails concernant le disque dur.

Quand on démarre un clonage ![Bouton de clonage](./img/jumping-gnu-48x48.png), on doit confirmer deux fois pour commencer le clonage vers le disque dur.

Le clonage commence alors. Il se déroule en quelques minutes, et il est possible d'en suivre la progression sur le moniteur. L'opération la plus longue est la copie de l'image du cœur du système, qui assure le démarrage du noyau Linux, et la présence de centaines de programmes, fournis par un système de fichiers compressé, en lecture-seule. Quand cette opération se termine, une partition dite de *persistance* est créée, c'est celle qui contiendra toutes les modifications apportées à la clé durant « sa vie » : les données visibles de l'utilisateur, ainsi que nombre de fichiers cachés, liés à des configurations, et aussi les nouveaux programmes qu'on désire y installer.

Quand l'opération de clonage se termine, il est possible d'enregistrer les messages des moniteurs à toutes fins utiles.
