© 2019 Georges Khaznadar <georgesk@debian.org>

This program is free, it is licensed under GPL-3+.

Live-Clone has been developed to allow users of
[live Freeduc sticks](https://usb.freeduc.org/jbart-en.html) to get
an auto-clone system, which makes easier to make them more secure
and to spread them.
