<style type="text/css">
h1 {
	color: darkred;
}

h2 {
	color: red;
}

h3 {
	color: blue;
}

p, ul, ol {
    margin-left: 30px;
    margin-right: 20px;
}

code, tt {
	font-family: Courier, fixed;
	font-weight: bold;
	color: brown;
}
</style>

# The toolbar ![toolbar](./img/toolbar1.png) #

* ![Help button](./img/help.png) The first button on the left of the toolbar provides a window "about" this application, and pulls this document on the top.
* The central application is cloning USB Freeduc to the hard disk, ![Clone button](./img/jumping-gnu-48x48.png) which can be triggered with the button or by a click one the title nearby.
* The last rightmost button, allows one to quit the application. ![Quit button](./img/application-exit.png) One can interrupt the application even when there is an ongoing command and monitors are displaying its details.

# Details of a cloning action #

In the display just underneath the toolbar, one can see details about the hard disk.

When one starts a cloning action ![Clone button](./img/jumping-gnu-48x48.png), one must confirm twice to begin cloning to the hard disk. 

The cloning action can begin after both confirmations. It takes some minutes, and one can watch its progress the monitor. The longest operation is the copy of the system's core, which will feature booting a Linux kernel, and some hundreds of programs, provided by a compressed read-only filesystem. When this operation is finished, a *persistence* partition is created; this one will contain every modification to the disk during its lifetime: data visible by the user, as well as a load of hidden files, like configurations, ans also every program one wants to install additionally.

When the cloning operation is finished, one can save the monitors' messages, for further use.
