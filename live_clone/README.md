LIVE-CLONE
==========

This GUI application allows on to clone Debian-Live USB sticks.
It has been developped to speed up cloning USB stickd of the
project **Freeduc** (Free Education), see https://usb.freeduc.org/jbart.html

Dependencies
------------

*  **Python3:** python3-markdown
*  **PyQt5:** python3-pyqt5
*  **GI:** Gio, GLib, UDisks, gir1.2-udisks-2.0

Screenshot
----------

Here is the window of the application, while one live USB disk is
plugged in the machine (``/dev/sdd``). Actions are available by the
toolbar buttons, and the help/user manual is fairly visible.

![screenshot of the main window](img/screen01.png)

Supported languages
-------------------

Currently supported languages are EN, FR.

Translations are welcome : most of the translatable strings are under the
``lang/`` subdirectory : ``*.md`` and ``*.ts`` files, and a few of them are in
files ``live_clone.desktop`` and ``org.freedesktop.live-clone.policy``.
