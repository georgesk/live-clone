lupdate_only {
  SOURCES +=  $$PWD/__init__.py \
              $$PWD/monitor.py \
              $$PWD/tools.py \
              $$PWD/wizards.py \
              $$PWD/ui_about.py \
              $$PWD/usbDisk2.py \
              $$PWD/makeLiveStick.py \
              $$PWD/hdmain.py \
              $$PWD/ui_live_clone.py \
              $$PWD/ui_diskinstall.py \
              $$PWD/ui_packageEditor.py
  }

TRANSLATIONS +=  lang/live_clone_fr.ts
