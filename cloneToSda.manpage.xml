<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [

<!--

`xsltproc -''-nonet \
          -''-param man.charmap.use.subset "0" \
          -''-param make.year.ranges "1" \
          -''-param make.single.year.ranges "1" \
          /usr/share/xml/docbook/stylesheet/docbook-xsl/manpages/docbook.xsl \
          cloneToSda.manpage.xml'

A manual page <package>.<section> will be generated. You may view the
manual page with: nroff -man <package>.<section> | less'. A typical entry
in a Makefile or Makefile.am is:

DB2MAN = /usr/share/sgml/docbook/stylesheet/xsl/docbook-xsl/manpages/docbook.xsl
XP     = xsltproc -''-nonet -''-param man.charmap.use.subset "0"

manpage.1: manpage.xml
        $(XP) $(DB2MAN) $<

The xsltproc binary is found in the xsltproc package. The XSL files are in
docbook-xsl. A description of the parameters you can use can be found in the
docbook-xsl-doc-* packages. Please remember that if you create the nroff
version in one of the debian/rules file targets (such as build), you will need
to include xsltproc and docbook-xsl in your Build-Depends control field.
Alternatively use the xmlto command/package. That will also automatically
pull in xsltproc and docbook-xsl.

Notes for using docbook2x: docbook2x-man does not automatically create the
AUTHOR(S) and COPYRIGHT sections. In this case, please add them manually as
<refsect1> ... </refsect1>.

To disable the automatic creation of the AUTHOR(S) and COPYRIGHT sections
read /usr/share/doc/docbook-xsl/doc/manpages/authors.html. This file can be
found in the docbook-xsl-doc-html package.

Validation can be done using: `xmllint -''-noout -''-valid manpage.xml`

General documentation about man-pages and man-page-formatting:
man(1), man(7), http://www.tldp.org/HOWTO/Man-Page/

-->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "Georges">
  <!ENTITY dhsurname   "Khaznadar">
  <!-- dhusername could also be set to "&dhfirstname; &dhsurname;". -->
  <!ENTITY dhusername  "georgesk">
  <!ENTITY dhemail     "georgesk@debian.org">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1) and
       http://www.tldp.org/HOWTO/Man-Page/q2.html. -->
  <!ENTITY dhsection   "8">
  <!-- TITLE should be something like "User commands" or similar (see
       http://www.tldp.org/HOWTO/Man-Page/q2.html). -->
  <!ENTITY dhtitle     "cloneToSda User Manual">
  <!ENTITY dhucpackage "CLONETOSDA">
  <!ENTITY dhpackage   "cloneToSda">
]>

<refentry>
  <refentryinfo>
    <title>&dhtitle;</title>
    <productname>&dhpackage;</productname>
    <authorgroup>
      <author>
       <firstname>&dhfirstname;</firstname>
        <surname>&dhsurname;</surname>
        <contrib>Wrote this manpage for the Debian system.</contrib>
        <address>
          <email>&dhemail;</email>
        </address>
      </author>
    </authorgroup>
    <copyright>
      <year>2020</year>
      <holder>&dhusername;</holder>
    </copyright>
    <legalnotice>
      <para>This manual page was written for the Debian system
        (and may be used by others).</para>
      <para>Permission is granted to copy, distribute and/or modify this
        document under the terms of the GNU General Public License,
        Version 2 or (at your option) any later version published by
        the Free Software Foundation.</para>
      <para>On Debian systems, the complete text of the GNU General Public
        License can be found in
        <filename>/usr/share/common-licenses/GPL</filename>.</para>
    </legalnotice>
  </refentryinfo>
  <refmeta>
    <refentrytitle>&dhucpackage;</refentrytitle>
    <manvolnum>&dhsection;</manvolnum>
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>
    <refpurpose>clone the Freeduc system from a live session to the machine's hard disk</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1 id="description">
    <title>DESCRIPTION</title>
    <para><command>&dhpackage;</command> allows one to clone the core of
    a live Freeduc system to the local machine's hard disk. All data on the
    hard disk will be erased, four partitions will be created on the hard disk
    </para>
    <variablelist>
      <varlistentry>
	<term>/dev/sda1</term>
	<listitem>
	  <para>
	    The first partition (which weighs some gigabytes) contains a
	    bootable iso-9660 filesystem, so the core of Freeduc is in
	    read-only mode.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>/dev/sda2	</term>
	<listitem>
	  <para>
	    The second partition is a hidden NTFS partition, inside the
	    iso-9660 partition, to feature iso-hybrid boot properties. This
	    partition contains necessary files to boot in EFI mode.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>/dev/sda3	</term>
	<listitem>
	  <para>
	    A vestigial vfat partition. For pluggable disks, it does make sense
	    to provide a vfat partition, which can contain files reachable
	    from Windows and Mac machines. When the same system is used on
	    a hard drive, allocating disk resource to this partition does not
	    make sense.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>/dev/sda4	</term>
	<listitem>
	  <para>
	    This partition bears a EXT4 filesystem. It is called the
	    "persistence" partition, and some of its directories are mounted
	    in overlay mode: that means that every changes to the original
	    system are written there.
	  </para>
	  <para>
	    If one erases the directory rw/ near the root of this partition,
	    all persistence data are removed. One important file must be
	    kept in its root directory: "persistence.conf" defines how
	    overlays are mounted. By default, this file defines one single
	    overlay for all the system.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
</refentry>

