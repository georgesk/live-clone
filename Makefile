DESTDIR =
SUBDIRS = doc  live_clone

%:
	for d in $(SUBDIRS); do \
	  $(MAKE) -C $$d $@ DESTDIR=$(DESTDIR);\
	done
