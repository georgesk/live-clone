.. Live-Clone documentation master file, created by
   sphinx-quickstart on Tue Dec 10 18:19:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation de Live-Clone
===========================

Live-Clone est une petite application dédiée pour cloner des clés vives
créées par le système live-build de Debian. Un exemple d'utilisation est
donné dans les clés USB Freeduc : voir
`usb.freeduc.org
<http://usb.freeduc.org/jbart.html>`_ à ce sujet.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: live_clone
	:members:

.. automodule:: live_clone.usbDisk2
	:members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
